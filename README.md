# api.kaki87.net

[![](https://shields.kaki87.net/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2Fserver-KaTys%2Fstatus%2Fmaster%2Fapi%2Fgeneral-api-ka-ki87%2Fuptime.json)](https://status.katys.cf/history/general-api-ka-ki87)
[![](https://shields.kaki87.net/discord/739600823415472128)](https://discord.gg/We7WfqGrzT)

100% FOSS & unlimited no-logs API

## Endpoints

### `GET` `/whois/{domain}`

#### Requirements

- `whois` linux package

#### Example URL

https://api.kaki87.net/whois/kaki87.net

#### Example result

```json
{
    "success": true,
    "data": {
        "domain name": [
            "kaki87.net"
        ],
        "registry domain id": [
            "1917812383_domain_net-vrsn"
        ],
        "registrar whois server": [
            "whois.ovh.com"
        ],
        "registrar": [
            "ovh sas"
        ],
        "registrar iana id": [
            "433"
        ],
        "registrar abuse contact email": [
            "abuse@ovh.net"
        ],
        "registrar abuse contact phone": [
            "+33.972101007"
        ],
        "name server": [
            "dns111.ovh.net",
            "ns111.ovh.net"
        ],
        "dnssec": [
            "signeddelegation"
        ],
        "dnssec ds data": [
            "47169 7 2 ac9912d2eb8a6391f3afd8b11638fa38e5ea0713847c9553f6fb4562dcc1c2cc"
        ],
        "registrant country": [
            "fr"
        ],
        "registrant email": [
            "e76wykeshqth514tqcv6@i.o-w-o.info"
        ],
        "admin email": [
            "c18djegvevqsvat21oep@v.o-w-o.info"
        ],
        "tech email": [
            "c18djegvevqsvat21oep@v.o-w-o.info"
        ]
    }
}
```

#### Known issues

- The regular expression used to sort relevant and irrelevant output lines from the `whois` command might hide relevant ones and/or show irrelevant ones. Don't hesitate to report these.

### `GET` `/dns/{domain}`

#### Requirements

- `dnsutils` linux package

#### Example URL

https://api.kaki87.net/dns/kaki87.net

https://api.kaki87.net/dns/kaki87.net?nameserver=9.9.9.9

#### Example result

```json
{
    "success": true,
    "data": [
        {
            "domain": "kaki87.net.",
            "ttl": "1530",
            "type": "A",
            "value": "62.210.109.120"
        }
    ]
}
```
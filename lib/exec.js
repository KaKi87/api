module.exports = cmd => new Promise((resolve, reject) => require('child_process').exec(cmd, ((error, stdout, stderr) => {
    if(!stdout || stderr)
        reject();
    else
        resolve(stdout);
})));
require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });

const fastify = require('fastify')({ trustProxy: true });

const {
    port,
    sentryDsn,
    matomoUrl,
    matomoSiteId,
    matomoToken
} = require('./config.json');

if(sentryDsn) fastify.register(require('fastify-sentry'), { dsn: sentryDsn });

if(matomoUrl && matomoSiteId && matomoToken) fastify.register(
    require('fastify-matomo'),
    { url: matomoUrl, siteId: matomoSiteId, token: matomoToken }
);

fastify.register(require('fastify-cors'));

fastify.register(require('./api/whois'));

fastify.register(require('./api/dns'));

fastify
    .listen(port, '0.0.0.0')
    .then(() => console.log('Server running'))
    .catch(console.error);
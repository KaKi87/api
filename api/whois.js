const exec = require('../lib/exec');

module.exports = async fastify => {
    fastify.get('/whois/:domain', async (req, res) => {
        const
            { domain } = req.params,
            isIp = /^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4}$/.test(domain),
            rootDomain = domain.match(/^(.+\.)?([^.]+\.[^.]+)$/)[2],
            experimental = typeof req.query['experimental'] === 'string';
        if(!isIp && domain !== rootDomain) return res.redirect(`/whois/${rootDomain}${experimental ? '?experimental' : ''}`);
        /** @type {String} */
        const output = await exec(`whois ${domain}`);
        const data = {};
        output.split(domain.endsWith('.gg') ? '\n\n' : '\n').forEach(line => {
            const match = line
                .toLowerCase()
                .match(/^ *([A-Z]?[a-z][^:]+):[\n ]+(.+?) *$/);
            if(!match) return;
            const
                key = match[1],
                value = match[2]
                .replace(/,/, '');
            if(/\b(terms|lawful|notice|remarks|url|here|you)\b/.test(key)) return;
            if(value.startsWith('http')) return;
            if(['to', 'website', 'use'].includes(key)) return;
            if(!experimental){
                if(!data[key])
                    data[key] = [];
                if(data[key].includes(value)) return;
                data[key].push(value);
            }
            else {
                let parentKey = data;
                const keys = key.split(' ');
                keys.forEach((currentKey, index) => {
                    if(index === keys.length - 1){
                        if(!Array.isArray(parentKey[currentKey]))
                            parentKey[currentKey] = [];
                        parentKey[currentKey].push(value);
                    }
                    else {
                        if(!parentKey[currentKey])
                            parentKey[currentKey] = {};
                        else if(Array.isArray(parentKey[currentKey])){
                            parentKey[currentKey] = {
                                $: parentKey[currentKey]
                            }
                        }
                    }
                    parentKey = parentKey[currentKey];
                });
            }
        });
        const success = !!Object.entries(data).length;
        return res.send({
            success,
            ...(success ? {
                data,
                ...(experimental ? {} : {
                    info: 'Try ?experimental'
                } )
            } : {
                raw: output
            })
        });
    });
};

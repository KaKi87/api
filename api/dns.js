const exec = require('../lib/exec');

module.exports = async fastify => {
    fastify.get('/dns/:domain', async (req, res) => {
        const { nameserver } = req.query;
        const command = `dig ${nameserver ? `@${nameserver}` : ''} ${req.params['domain']} ANY +noall +answer`;
        const output = `${await exec(command)}\n${await exec(command.replace('ANY', ''))}`;
        res.send({
            success: true,
            data: output
            .split('\n')
            .filter(line => !!line && !line.startsWith(';'))
            .map(line => line.split(/\s+/g))
            .map(chunk => ({
                domain: chunk[0],
                ttl: parseInt(chunk[1]),
                type: chunk[3],
                priority: /^[0-9]+$/.test(chunk[4]) ? parseInt(chunk[4]) : undefined,
                value: chunk.slice(/^[0-9]+$/.test(chunk[4]) ? 5 : 4).join(' ')
            }))
        });
    });
};